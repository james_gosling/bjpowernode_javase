
# 方法覆盖 `Override`/`Overwrite`
**⏰ 提醒**  
可以回顾下 [`方法重载`](./007%20%E6%96%B9%E6%B3%95.md)  

## 覆盖发生的前提条件
* 具备继承关系的两个类  
* **返回值类型<sup>⚠️ 这个其实可以不一致，具备继承关系也行得通</sup>**、**方法名**、**参数列表**一致  
🙋 **老杜补充:**  
覆盖后的方法可以使用 *父类方法返回值类型的**子类***  
如果返回值类型是 *<u>基本数据类型</u>* ，则需要**严格保持一致**  
* 🙋 **访问权限** 不能更低，可以`更高`  
* 🙋 重写后的方法 **不能** 比继承过来的抛出 `更广泛` 的异常  

## 额外注意事项<sup> 与 [`多态`](#多态) 相关</sup>
* 🙋 私有方法无法被覆盖  
* 构造方法不会被继承，所以也没有“覆盖”的说法  
* 方法覆盖只针对 **实例方法**，静态方法覆盖没有意义  
因为*静态方法*一般是使用 `类名.` 的方式来调用的  
```
Class Animal {
    public static void bark() {
        System.out.println("Animal barks");
    }
}

Class Cat extends Animal {
    public static void bark() {
        System.out.println("Cat meow");
    }
}

Public Class OverrideSample {
    public static void main(String[] args) {
        Animal a = new Cat();
        a.bark();   // 这里最终调用的是Animal的bark方法
    }
}
```

# <a name="polymorphism">多态</a>
